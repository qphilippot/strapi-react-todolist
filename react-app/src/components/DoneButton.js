import React, { Component } from 'react';

class DoneButton extends Component {
    constructor(props) {
        super(props);

        this.state = {
            id: props.task.id,
            name: props.task.name,
            status: props.task.status
        }
    }

    render() {
        return (
            <button
                className="snipcart-add-item BuyButton"
                data-item-id={this.state.id}
                data-item-name={this.state.name}
                data-item-status={this.state.status}
            >
            </button>
        );
    }

    
}

export default DoneButton;