import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import DoneButton from './DoneButton';

class TaskList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      tasks: []
    }
  }

  async componentDidMount() {
    let response = null;
    
    try {
      response = await fetch("http://localhost:1337/task");
    }

    catch(error) {
      console.error(error);
    }

    if (
      response === null || 
      response.ok === false
    ) {
      return;
    }

    let tasks = await response.json()
    this.setState({ loading: false, tasks: tasks })
  }

  render() {
    if (!this.state.loading) {
      return (
        <div className="TaskList">
          <h2 className="TaskList-title">Pending Tasks... ({this.state.tasks.length})</h2>
          <div className="TaskList-container">
            {this.state.tasks.map((task, index) => {
              return (
                <div className="TaskList-task" key={task.id}>
                  <Link to={`/task/${task.id}`}>
                    <h3>{task.name}</h3>
                  </Link>
                  <DoneButton task={task} />
                </div>
              );
            })}
          </div>
        </div>
      );
    }

    return (<h2 className="TaskList-title">Waiting for API...</h2>);
  }
}

export default TaskList;