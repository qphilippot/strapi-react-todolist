import React, { Component } from 'react';
import DoneButton from './DoneButton';


class Task extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: true,
            tasks: {}
        };
    }

    async componentDidMount() {
        const response = await fetch(`http://localhost:1337/task/${this.props.match.params.id}`);
        const data = await response.json();

        this.setState({
            loading: false,
            task: data
        })
    }

    render() {
        if (this.state.loading === false) {
            return(
                <div className="task">
                <div className="task__information">
                  <h2 className="task-title">
                    {this.state.task.name}
                  </h2>
                  <DoneButton {...this.state} />
                </div>
                <div className="task__description">
                  {this.state.task.description}
                </div>
              </div>
            );
        }

        else {
            return (
                <h2>Waiting for API ...</h2>
            );
        }
    }
}

export default Task;