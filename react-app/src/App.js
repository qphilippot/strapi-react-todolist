import React from 'react';
import { Route, Switch } from 'react-router';
import { Link } from 'react-router-dom';
import logo from './logo.svg';
import './App.css';

import TaskList from './components/TaskList';
import Task from './components/Task';

function App() {
  return (
    // <div className="App">
    //   <header className="App-header">
    //     <img src={logo} className="App-logo" alt="logo" />
    //     <p>
    //       Edit <code>src/App.js</code> and save to reload.
    //     </p>
    //     <p>Hello react</p>
    //     <a
    //       className="App-link"
    //       href="https://reactjs.org"
    //       target="_blank"
    //       rel="noopener noreferrer"
    //     >
    //       Learn React
    //     </a>
    //   </header>
    // </div>
    <div className="App">
        <Header />
        <main className="App-content">
          <Switch>
            <Route
              path="/" exact 
              component={TaskList} 
            />
            
            <Route 
              path="/task/:id" 
              component={Task} 
            />
          </Switch>
        </main>
      <Footer />
    </div >
  );
}

export default App;

const Header = () => {
  return (
    <header className="App-header">
      <Link to="/"><h1>🦐 Exotic Fish Supplies</h1></Link>
      <div className="right">
        <button className="snipcart-checkout snipcart-summary">
          Checkout (<span className="snipcart-total-items"></span>)
      </button>
      </div>
    </header>
  );
}

const Footer = () => {
  return (
    <footer></footer>
  );
}